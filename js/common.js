﻿
$(document).ready(function () {

	initProfile();
	//powerKidsCommon.init();
});

function initProfile() {
	if ($(".playerMenuProfile").length == 0) return;
	
	$(document).bind('player.update_menu-profile', function (e) {
		$.ajax({
			url: "/api/player",
			type: "get",
			data: {},
			success: function(data){
				if (!data)
				{

				}
				else 
				{
					$(".playerMenuProfile").find(".ebPoints").html(data.player_data.eb);
				}
			}
		});  
		
	});
}





jQuery.unparam = function (value) {
    var
    // Object that holds names => values.
    params = {},
    // Get query string pieces (separated by &)
    pieces = value.split('&'),
    // Temporary variables used in loop.
    pair, i, l;

    // Loop through query string pieces and assign params.
    for (i = 0, l = pieces.length; i < l; i++) {
        pair = pieces[i].split('=', 2);
        // Repeated parameters with the same name are overwritten. Parameters
        // with no value get set to boolean true.
        params[decodeURIComponent(pair[0])] = (pair.length == 2 ?
            decodeURIComponent(pair[1].replace(/\+/g, ' ')) : true);
    }

    return params;
};

function stringToArray(data, splitChar) {
    //console.log("called stringToArray()");
    //console.log("split: " + data);
    if (data == "" || data == undefined) return [];
    var arr = data.split(splitChar);
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Number(arr[i]);
        //console.log(i + " / " + arr[i]);
    }
    //console.log("arr = " + arr);
    return arr;
}

function stringToBoolean(string) {
    switch(string.toLowerCase()){
        case "true": case "yes": case "1": return true;
        case "false": case "no": case "0": case null: return false;
        default: return Boolean(string);
    }
}

function setCanvasResolution() {
    return;
    // Setup canvas size and update logger display
    //console.log("canvas2 = " + canvas);
    canvas.width = $(window).width();
    canvas.height = $(window).height();
    screen_width = canvas.width;
    screen_height = canvas.height;
}

Number.prototype.formatMoney = function (c, d, t, currency) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    if (currency == "EUR") {
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "") + " EUR";
    } else {
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "") + " €";
    }
}


function checkStr(val) {
    if (val == undefined || val == null) {
        return "";
    } else {
        return String(val);
    }
}

function checkNum(val, returnVal) {
    if (isNaN(val)) {
        if (isNaN(returnVal)) {
            return 0;
        } else {
            return returnVal;
        }
    } else {
        return Number(val);
    }
}

function declinateScores(s) {
    switch (s) {
        case 0:
            return s + " točk";
            break;
        case 1:
            return s + " točka";
            break;
        case 2:
            return s + " točki";
            break;
        case 3:
        case 4:
            return s + " točke";
            break;
        default:
            return s + " točk";
            break;
    }
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}



