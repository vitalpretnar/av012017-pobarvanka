$(document).ready(function() {
	coloring.init();

	$("#submit").click(function(e) {
		if (!game.is_posted) {
			game.is_posted = true;
			postData();
		}
	})

})


var submitPermisions = {
	bgSet: false,
	colored: false,
	isEnabled: false,
	check : function() {
		if (this.bgSet && this.colored && !this.isEnabled) {
			this.isEnabled = true;
			$("#btn-end").addClass("enabled");
			$("#btn-end").click(function(e) {
				$("#scene-coloring").fadeOut("fast", function() {
					$("#scene-form").fadeIn("fast");
				})
			})
		}
	}
}

var coloring = {};


coloring.init = function() {
	this.allowedSurfaces = 'clr-body,clr-head,clr-beak-2,clr-beak-1,clr-tail,clr-shoes,clr-legs,clr-crest,clr-wing-right,clr-fingers-right,clr-fingers-right-2,clr-wing-left,clr-fingers-left';
	this.currentFill = "#FFEC5F";
	this.parrotPosition = {left:0,top:0};
	this.season = "default";
	this.isPosted = false;

	$('#cp-swatches').click(function(e) {
		this.currentFill = $(e.target).attr("fill");
		$("#cp-swatches rect").removeClass("selected");
		$(e.target).addClass("selected");
	}.bind(this))
	

	$("#coloring-content svg").click(function (event) { 
	    if (coloring.allowedSurfaces.indexOf($(event.target).attr('id'))>=0) {
			submitPermisions.colored = true;
	    	submitPermisions.check();
	    	$(event.target).attr('style', 'fill:' + coloring.currentFill + " !important");
	    	coloring.save();
	    } 
	})


	$(".btn-bg").click(function (event) { 
		submitPermisions.bgSet = true;
    	submitPermisions.check();
		$(".btn-bg").removeClass("selected");
		$(event.target).addClass("selected");
		coloring.season = $(event.target).attr('id').replace("btn-", "");
		$("#coloring-content").css( "background-image", "url(assets/bg-" + coloring.season + ".jpg)" );
		coloring.save();
	})

	//coloring.paint();
	coloring.save();
}

coloring.paint = function(data) {
	var test = '{"parrot":{"season":"fall","colors":{"clr-body":"rgb(255, 236, 95)","clr-head":"rgb(179, 179, 179)","clr-beak-2":"rgb(204, 204, 204)","clr-beak-1":"rgb(255, 236, 95)","clr-tail":"rgb(153, 153, 153)","clr-shoes":"rgb(204, 204, 204)","clr-legs":"rgb(230, 230, 230)","clr-crest":"rgb(230, 230, 230)","clr-wing-right":"rgb(179, 179, 179)","clr-fingers-right":"rgb(230, 230, 230)","clr-fingers-right-2":"rgb(204, 204, 204)","clr-wing-left":"rgb(204, 204, 204)","clr-fingers-left":"rgb(230, 230, 230)"}}}';
	var cd = data || test;
	cd = jQuery.parseJSON(cd);
	for (var item in cd.parrot.colors) {
		$("#" + item).css( "fill", cd.parrot.colors[item] );
	}
	if (cd.parrot.season != "default") $("#coloring-content").css( "background-image", "url(assets/bg-" + cd.parrot.season + ".jpg)" );
	
}

coloring.save = function() {
	var arr = this.allowedSurfaces.split(',');
	var result = { 
		'parrot' : {
			'season' : this.season,
			'colors': {} 
		}
	}
	for (var i=0; i<arr.length; i++) {
		result.parrot.colors[arr[i]] = $('#' + arr[i]).css('fill');
	}
	$('#coloring-data').val(JSON.stringify(result));
}

$.ajaxSetup({
    type: "POST",
    url: "http://qlandia2016.masa.av-studio.net/si/nagradne-igre/2017/pobarvanka?ajaxcall=true",
    dataType: 'text',
    xhrFields: {
        withCredentials: true
    },
    crossDomain: true,
    beforeSend: function (xhr) {
        //xhr.setRequestHeader("Cookie", "session=xxxyyyzzz");
    },
    error: function (xhr) {
//        wrapper.error(t.error + " / t004");
    }
});

function postData() {
    $.ajax(
        {
            data: {
                name: $("#firstname").val(),
                lastname: $("#lastname").val(),
                email: $("#email").val(),
                address: $("#address").val(),
                city: $("#city").val(),
                rules: $("#rules").is(':checked'),
                imagedata: $("#coloring-data").val(),
            },
            success: function (dataStr) {
            	var data = $.parseJSON(dataStr);
                if (data.error) {
                	$("#instructions").html("Za sodelovanje morajo biti vsa polja izpolnjena pravilno in strinjati se morate s pogoji sodelovanja.");
                	coloring.isPosted = false;
                } else {
                	$("#form").html("Hvala za sodelovanje!");
                }
            }
        }
    );
}


